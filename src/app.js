const SqsAdapter = require('./sqsAdapter');
const events = require('events');
const Configurapi = require('configurapi');
const { SQSClient, ReceiveMessageCommand, DeleteMessageCommand } = require('@aws-sdk/client-sqs');

module.exports = class SqsRunner extends events.EventEmitter 
{
    constructor() 
    {
        super();

        this.queueUrl = undefined;
        this.errorQueueUrl = undefined;
        this.responseQueueUrl = undefined;
        this.concurrencyCount = 10;
        this.configPath = "config.yaml";
        this.service = undefined;
        this.config = {errorQueueUrl: undefined, responseQueueUrl: undefined};
        this.stopRequested = false;
        this.counter = 0;
        this.region = undefined;
        this.pollingWaitTime = 0;
    }

    run(options) 
    {
        this._handleOptions(options);

        let config = Configurapi.Config.load(this.configPath);

        this.service = new Configurapi.Service(config);
        this.service.on("trace", (s) => this.emit("trace", s));
        this.service.on("debug", (s) => this.emit("debug", s));
        this.service.on("error", (s) => this.emit("error", s));

        let sqs = new SQSClient({ 
            endpoint: new URL(this.queueUrl).origin,
            useQueueUrlAsEndpoint: true, 
            region: this.region 
        });

        const receiveParams = {
            QueueUrl: this.queueUrl,
            MaxNumberOfMessages: this.concurrencyCount,
            WaitTimeSeconds: 20
        };
        const receiveCommand = new ReceiveMessageCommand(receiveParams);

        let deleteMessage = async (receiptHandle) =>
        {
            const deleteParams = {
                QueueUrl: this.queueUrl,
                ReceiptHandle: receiptHandle,
            };
            const deleteCommand = new DeleteMessageCommand(deleteParams);
            await sqs.send(deleteCommand);
        }
        

        let poll = async () =>
        {
            if(this.stopRequested) return;

            let response = await sqs.send(receiveCommand)
            
            if (response?.Messages && response?.Messages.length > 0) 
            {
                for(let message of response.Messages)
                {
                    this.counter++;
                    
                    try
                    {
                        await this._requestListener(message)
                    }
                    finally
                    {
                        this.counter--;
                        await deleteMessage(message.ReceiptHandle);
                    }
                }
            }
            
            await new Promise((resolve)=>setTimeout(resolve, this.pollingWaitTime));

            await poll();
        }

        this.emit("trace", "Server is listening...");

        poll().catch(err => this.emit("error", err.toString()))        
    }

    stop()
    {
        return new Promise(async (resolve, reject) => 
        {
            this.stopRequested = true

            const sleep = (ms) => new Promise((resolve2, reject2) => setTimeout(resolve2, ms));

            while(this.counter > 0) 
            {
                this.emit("trace", `Server is stopping...[${this.counter}]`);
                await sleep(500);
            }

            this.emit("trace", 'Server is stopped.');
            
            this.stopRequested = false;
            return resolve();
            
        });
    }

    _handleOptions(options) 
    {
        if (!options) return;

        if ('queueUrl' in options) this.queueUrl = options.queueUrl;
        if ('errorQueueUrl' in options) this.errorQueueUrl = options.errorQueueUrl;
        if ('responseQueueUrl' in options) this.responseQueueUrl = options.responseQueueUrl;
        if ('configPath' in options) this.configPath = options.configPath;
        if ('region' in options) this.region = options.region;
        if ('concurrencyCount' in options && options['concurrencyCount']) this.concurrencyCount = options.concurrencyCount;
        if ('pollingWaitTime' in options) this.pollingWaitTime = options.pollingWaitTime;
        this.config = {errorQueueUrl: this.errorQueueUrl, responseQueueUrl: this.responseQueueUrl, region: this.region};
    }

    async _requestListener(incomingMessage) 
    {
        try {
            let event = new Configurapi.Event(await SqsAdapter.toRequest(incomingMessage));

            await this.service.process(event);
            
            await SqsAdapter.write(event.id, event.response, this.config);
        }
        catch (error) {
            let response = new Configurapi.ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

            try
            {
                await SqsAdapter.write('', response, this.config);
            }
            catch(err)
            {
                try
                {
                    this.emit("error", JSON.stringify(err));
                }
                catch(errorFromListner)
                {
                    console.log(errorFromListner);
                }
            }
        }
    }
};
