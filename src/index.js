#!node

const { program } = require('commander');
const App = require('./app');
const oneliner = require('one-liner');

///////////////////////////////////
// Utility functions
///////////////////////////////////
function log(s)
{
    if(process.env.LOG_LEVEL==='none') return;

    process.stdout.write(s);
}
function format(prefix, str)
{   
    let time = new Date().toISOString();
    return `${time} ${prefix}: ${oneliner(str)}\n`;
}
function logTrace(s)
{   
   log(format('Trace', s));
}
function logError(s)
{
    log(format('Error', s));
}
function logDebug(s)
{
    log(format('Debug', s));
}

///////////////////////////////////
// Process arguments
///////////////////////////////////
program.option('-q, --queue [url]', 'SQS queue url for incoming messages')
         .option('-e, --error [url]', 'SQS queue url for error messages')
         .option('-r, --response [url]', 'SQS queue url for response messages')
         .option('-f, --file [path]', 'Path to the config file')
         .option('-g, --region [string]', 'AWS Region')
         .option('-c, --concurrency [number]', 'Concurrenncy level')
         .option('-w, --wait [number]', 'Time to wait in ms after each polling')
         .parse(process.argv);

let options = program.opts();
let queueUrl   = options.queue || process.env.CONFIGURAPI_SQS_REQUEST_QUEUE;
let configPath = options.file ? options.file : 'config.yaml';
let errorQueueUrl   = options.error || process.env.CONFIGURAPI_SQS_ERROR_QUEUE;
let responseQueueUrl   = options.response || process.env.CONFIGURAPI_SQS_RESPONSE_QUEUE;
let concurrency = options.concurrency || process.env.CONFIGURAPI_SQS_CONCURRENCY_COUNT;
let region = options.region || process.env.AWS_REGION || 'us-east-1';
let pollingWaitTime = options.wait || process.env.CONFIGURAPI_POLLING_WAIT_TIME || 0;

if(!queueUrl)
{
    program.help();
    process.exit(1);
}

///////////////////////////////////
// Start the API
///////////////////////////////////
let app = new App();
app.on('error', (s) => {logError(s);});
app.on('debug', (s) => {logDebug(s);});
app.on('trace', (s) => {logTrace(s);});

process.on('SIGTERM', async () => {
    await app.stop();
    process.exit(0);
});

process.on('SIGINT', async () => {
    await app.stop();
    process.exit(0);
});

app.run({queueUrl: queueUrl, errorQueueUrl: errorQueueUrl, responseQueueUrl: responseQueueUrl, configPath:configPath, concurrencyCount: concurrency, region: region, pollingWaitTime: pollingWaitTime});