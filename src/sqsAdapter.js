const Configurapi = require('configurapi');
const { SQSClient, SendMessageCommand } = require('@aws-sdk/client-sqs');

async function enqueue(queueUrl, data)
{
    const sqs = new SQSClient({
        endpoint: new URL(queueUrl).origin,
        useQueueUrlAsEndpoint: true,
        region: process.env.AWS_REGION || 'us-east-1'
    });

    const params = {
        QueueUrl: queueUrl,
        MessageBody: JSON.stringify(data)
    };
    
    const command = new SendMessageCommand(params);

    await sqs.send(command);
}

module.exports = 
{
    write: async function(eventId, response, config)
    {
        if(response instanceof Configurapi.ErrorResponse && config.errorQueueUrl)
        {
            return await this.writeError(eventId, response, config);
        }

        if(config.responseQueueUrl)
        {
            return await this.writeResponse(eventId, response, config);
        }
    },

    writeError: async function(eventId, response, config)
    {
        await enqueue(config.errorQueueUrl, {
            'name': 'error',
            'payload': response
        },
        config);
    },

    writeResponse: async function(eventId, response, config)
    {
        if(response.body instanceof String)
        {
            body = response.body;
        }
        else if(response.body instanceof Object)
        {
            let jsonReplacer = 'jsonReplacer' in response ? response.jsonReplacer : undefined;

            body = JSON.stringify(response.body, jsonReplacer);
        }
        else if(response.body instanceof Number)
        {
            body = response.body + '';
        } 
        else 
        {
            body = response.body;
        }

        await enqueue(config.responseQueueUrl, {
            'name': 'response',
            'payload': response
        },
        config);
    },

    toRequest: async function(incomingMessage)
    {
        let request = new Configurapi.Request();
        
        let data = JSON.parse(incomingMessage.Body);

        request.method = '';
        request.headers = data.headers || {};
        request.name =  data.name || undefined;
        request.query = data.query || {};
        request.payload = data.payload || undefined;
        
        return request;
    }
};
